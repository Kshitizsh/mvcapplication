﻿using System;
using TechTalk.SpecFlow;
using SampleDemo1.PageObjects;
using OpenQA.Selenium;

namespace SampleDemo1.StepDefinitions
{
    [Binding]
    public class TestSteps : StepDefinitionBase
    {
        [Given(@"I am on google search page")]
        public void GivenIAmOnGoogleSearchPage()
        {
            //ScenarioContext.Current.Pending();
            Browser.Navigate().GoToUrl(Uri);

            System.Threading.Thread.Sleep(10000);
        }
    }
}
