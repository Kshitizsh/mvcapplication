﻿using System;
using TechTalk.SpecFlow;
using SampleDemo1.PageObjects;
using OpenQA.Selenium;
using RestSharp;

namespace SampleDemo1.StepDefinitions
{
	[Binding]
	public class Get_API_ValuesSteps : StepDefinitionBase
	{
		[AfterFeature]
		public static void updateJIRA()
		{
			var client = new RestClient("http://localhost:8081/rest/zapi/latest");
			// client.Authenticator = new HttpBasicAuthenticator(username, password);

			var request = new RestRequest("/execution", Method.POST);

			// easily add HTTP Headers
			request.AddHeader("Authorization", "Basic YWRtaW46cWVudGVsbGkxMjM=");
			//request.AddHeader("Authorization", "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInFzaCI6ImRjOGUzNzA2OWVkZmIxNGM1MDZlZTQ3ZTFjNDQ4MGI1MmQxMDU4ZTQ0ZTI4NGEyMDc5YTMwNTczNmQyNDlhMGIiLCJpc3MiOiJNMlpqT0Rjd09EQXROemxtWlMwek4yTTVMV0ZtT0RNdE9EZGpOMlpoWWpobE1UUm1JR0ZrYldsdUlHRmtiV2x1IiwiZXhwIjoxNTExNzc5ODc1LCJpYXQiOjE1MTE3NzYyNzV9.khHsqJu7xAb6sSgkfpymuDVSj3hwM_5Xm-GjBQsxbJI");
			//request.AddHeader("zapiAccessKey", "l17P9cIuPaX-eVjskPXB2RDmhyV907RS8ytm4ikZLLo");
			request.AddHeader("Content-Type", "application/json");
			//request.AddHeader("User-Agent", "C# App");

			//
			//object x = "{\"projectId\":10000,\"versionId\":-1,\"issueId\":10000}";
			request.AddJsonBody(new
			{
				projectId = 10000,
				versionId = -1,
				issueId = FeatureContext.Current["issueId"]
			});
			// execute the request
			IRestResponse response = client.Execute(request);
			var content = response.Content; // raw content as string
			var x = content.Split('{')[1].Split(':')[0].Split('"')[1];
			bool status = (bool)FeatureContext.Current["status"];
			if (status)
			{
				request = new RestRequest("/execution/" + x + "/execute", Method.PUT);
				request.AddHeader("Authorization", "Basic YWRtaW46cWVudGVsbGkxMjM=");
				request.AddHeader("Content-Type", "application/json");
				request.AddJsonBody(new
				{
					status = 1
				});
				client.Execute(request);
			}
			else
			{
				request = new RestRequest("/execution/" + x + "/execute", Method.PUT);
				request.AddHeader("Authorization", "Basic YWRtaW46cWVudGVsbGkxMjM=");
				request.AddHeader("Content-Type", "application/json");
				request.AddJsonBody(new
				{
					status = 2
				});
				client.Execute(request);
			}
		}
		[Given(@"I launch the Test Application")]
		public void GivenILaunchTheTestApplication()
		{
			FeatureContext.Current.Add("status", true);
			Browser.Navigate().GoToUrl(Uri);
			var homePage = new HomePage(Browser);
			homePage.waitForHomePageLoad();
		}

		[Given(@"I click on API menu")]
		public void GivenIClickOnAPIMenu()
		{
			System.Threading.Thread.Sleep(5000);
			var homePage = new HomePage(Browser);
			homePage.NavigateToPage("API");
		}

		[When(@"I click on get API values")]
		public void WhenIClickOnGetAPIValues()
		{
			FeatureContext.Current.Add("issueId", 10000);
			System.Threading.Thread.Sleep(5000);
			var apiPage = new ApiPage(Browser);
			apiPage.selectLink("Get_Values");
		}

		[When(@"I click on Post API values")]
		public void WhenIClickOnPostAPIValues()
		{
			FeatureContext.Current.Add("issueId", 10007);
			System.Threading.Thread.Sleep(5000);
			var apiPage = new ApiPage(Browser);
			try
			{
				apiPage.selectLink("Pos_Values");
			}
			catch (Exception e)
			{
				FeatureContext.Current["status"] = false;
			}
		}

		[Then(@"the API Help Page is displayed")]
		public void ThenTheAPIHelpPageIsDisplayed()
		{
			String pageTitle = Browser.Title;
			if (pageTitle.Contains("API Help Page"))
			{


			}
			else
			{
				//Write code to create a new issue in Rally if it does not exists.

				FeatureContext.Current["status"] = false;
			}
		}

		[Then(@"the response information page is displayed")]

		public void ThenTheResponseInformationPageIsDisplayed()
		{
			System.Threading.Thread.Sleep(5000);
			String pageTitle = Browser.Title;
			if (pageTitle.Contains("api"))
			{


			}
			else
			{

				FeatureContext.Current["status"] = false;
				throw new Exception("API page is not displayed");
			}

		}


		[Then(@"the resposne information page is displayed")]
		public void ThenTheResposneInformationPageIsDisplayed()
		{
			System.Threading.Thread.Sleep(5000);
			String pageTitle = Browser.Title;
			if (pageTitle.Contains("api"))
			{


			}
			else
			{

				FeatureContext.Current["status"] = false;
				throw new Exception("API page is not displayed");
			}

		}

		[Then(@"I save the response information in Json format")]
		public void ThenISaveTheResponseInformationInJsonFormat()
		{
			var apiPage = new ApiPage(Browser);
			String jSonInfo = apiPage.getJSONResponse();
			if (jSonInfo.Length > 0)
			{


			}
			else
			{

				FeatureContext.Current["status"] = false;
				throw new Exception("Json format");
			}


		}
	}
}
