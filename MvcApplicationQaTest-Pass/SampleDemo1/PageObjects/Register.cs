﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using TechTalk.SpecFlow;
using System.Windows.Forms;

namespace SampleDemo1.PageObjects
{
    public class Register : Page
    {
        public static string link = "";
        private static class Locator
        {
            public static readonly By RegisterFormButton = By.LinkText("register an account?");            
            public static readonly By FirstName = By.Name("firstname");
            public static readonly By LastName = By.Name("lastname");
            public static readonly By Email = By.Id("loginradius-raas-registration-emailid");
            public static readonly By PostalCode = By.Name("PostalCode");
            public static readonly By Password = By.Id("loginradius-raas-registration-password");
            public static readonly By ConfirmPassword = By.Name("confirmpassword");
            public static readonly By RegisterSubmitButton = By.Id("loginradius-raas-submit-Register");

            public static readonly By Message = By.XPath("//p[@id='messageinfo']");
            public static readonly By SuccessMessage = By.XPath("//p[@id='successMessageinfo']");

            public static readonly By VerifyEmail = By.Id("inboxfield");
            public static readonly By MailinatorSubmitButton = By.XPath("//button[contains(text(),'Go!')]");
            public static readonly By FindVerificationEmail = By.XPath("//div[contains(text(),'My World Vision')]");
            public static readonly By FindVerificationLink = By.CssSelector("font>a:nth-child(7)");
            public static readonly string LinkFrame = "publicshowmaildivcontent";
        }

        public Register(IWebDriver browser)
            : base(browser)
        {
            
        }

        public bool ClickRegisterButton()
        {
           Do(x => x.Click(), Locator.RegisterFormButton, true);
           Page.waitTillElementisDisplayed(browser, Locator.FirstName, 10);
           return true;           
        }

        public bool ProvideRegisterDetails(string firstName, string lastName, string Email, string postalCode, string password, string confirmPassword)
        {          
            Do(x => x.SendKeys(firstName), Locator.FirstName, true);
            Do(x => x.SendKeys(lastName), Locator.LastName, true);
            Do(x => x.SendKeys(Email), Locator.Email, true);
            Do(x => x.SendKeys(postalCode), Locator.PostalCode, true);
            Do(x => x.SendKeys(password), Locator.Password, true);
            Do(x => x.SendKeys(confirmPassword), Locator.ConfirmPassword, true);
            MessageBox.Show("Waiting to handle the CAPTCHA manually");           
           
            return true;
        }

        public void submitRegistration()
        {
            Do(x => x.Click(), Locator.RegisterSubmitButton, true);

            System.Threading.Thread.Sleep(5000);
        }

        public bool completeRegistration(string msg)
        {
           // System.Threading.Thread.Sleep(5000);

            if (Page.waitTillElementisDisplayed(browser,Locator.Message,10))
            {
                string actualmsg=browser.FindElement(Locator.Message).Text;

                if(actualmsg.Contains(msg))
                {
                    return true;
                }
                else
                {
                    throw new Exception("The expected message is not displayed. Expected:" + msg + ", Actual:" + actualmsg);
                }
            }
            else
            {
                throw new Exception("The expected message is not displayed");
            }
        }
        public bool verifySuccessMsg(string msg)
        {
            // System.Threading.Thread.Sleep(5000);

            if (Page.waitTillElementisDisplayed(browser, Locator.SuccessMessage, 15))
            {
                string actualmsg = browser.FindElement(Locator.SuccessMessage).Text;

                if (actualmsg.Contains(msg))
                {
                    return true;
                }
                else
                {
                    throw new Exception("The expected message is not displayed. Expected:" + msg + ", Actual:" + actualmsg);
                }
            }
            else
            {
                throw new Exception("The register success message is not displayed. An account already exists for this email address. ");
            }
        }
        public bool verifyEmailAddress(string verifyEmail)
        {
            if (Page.waitTillElementisDisplayed(browser, Locator.VerifyEmail, 5))
            {
                Do(x => x.SendKeys(verifyEmail), Locator.VerifyEmail, true);
                Do(x => x.Click(), Locator.MailinatorSubmitButton, true);

                System.Threading.Thread.Sleep(2000);

                Do(x => x.Click(), Locator.FindVerificationEmail, true);

                System.Threading.Thread.Sleep(5000);

                browser.SwitchTo().Frame(Locator.LinkFrame);
                link = browser.FindElement(By.XPath("//font/a")).Text;

                
                return true;
            }
            else
            {
                throw new Exception("Error while opening Mailinator...");
            }
          
        }

        public bool navigateToConfLink()
        {
            if (link.Equals(""))
            {
                throw new Exception("Did not get the confirmation link to navigate to");
            }
            browser.Navigate().GoToUrl(link);
            return true;
        }

        
    }
}
