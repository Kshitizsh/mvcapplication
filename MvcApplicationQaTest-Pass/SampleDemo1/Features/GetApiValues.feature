﻿Feature: Get_API_Values


@FetchAPI
Scenario: Get API Values
Given I launch the Test Application
And I click on API menu
Then the API Help Page is displayed
When I click on get API values
Then the response information page is displayed
And I save the response information in Json format