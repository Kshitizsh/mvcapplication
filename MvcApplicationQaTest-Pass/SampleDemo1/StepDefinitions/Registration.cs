﻿using System;
using TechTalk.SpecFlow;
using SampleDemo1.PageObjects;
using OpenQA.Selenium;

namespace SampleDemo1.StepDefinitions
{
    [Binding]
    public class Registration : StepDefinitionBase
    {
        [Given(@"I am on register page")]
        public void GivenIAmOnRegisterPage()
        {
            Browser.Navigate().GoToUrl(Uri);
        }


        [Given(@"Click on register button")]
        public void GivenClickOnRegisterButton()
        {
            var registerFeature = new Register(Browser);
            registerFeature.ClickRegisterButton();
        }


        [Given(@"I provided required details ""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)""")]
        public void GivenIProvidedRequiredDetails(string firstName, string lastName, string Email, string postalCode, string password, string confirmPassword)
        {
            var registerFeature = new Register(Browser);
            registerFeature.ProvideRegisterDetails(firstName, lastName, Email, postalCode, password, confirmPassword);            
            
        }

        [When(@"I press register")]
        public void WhenIPressRegister()
        {
            var registerFeature = new Register(Browser);
            registerFeature.submitRegistration();
        }


        [Then(@"I should see banner with message ""(.*)""")]
        public void ThenIShouldSeeRegisterSuccessBannerWithMessage(string msg)
        {
           var registerFeature = new Register(Browser);
           registerFeature.completeRegistration(msg);
        }

        [Then(@"I should see banner with success message ""(.*)""")]
        public void ThenIShouldSeeBannerWithSuccessMessage(string msg)
        {
            var registerFeature = new Register(Browser);
            registerFeature.verifySuccessMsg(msg);
        }

        

        [Given(@"I receive a mail with email confirmation link ""(.*)"", ""(.*)""")]
        public void GivenIReceiveAMailWithEmailConfirmationLink(string emailDomain, string email)
        {
            Browser.Navigate().GoToUrl(emailDomain);

            var registerFeature = new Register(Browser);
            registerFeature.verifyEmailAddress(email);
        }

        

       
    }
}
