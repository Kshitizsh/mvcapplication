﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcApplicationTest.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2","value3" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

       public int AddNumbers(int pintFirstNumber, int pintSecondNumber)
        {
            return pintFirstNumber + pintSecondNumber;
        }

        public int SubractNumbers(int pintFirstNumber, int pintSecondNumber)
        {
            return pintFirstNumber - pintSecondNumber;
        }

        public int DivideNumbers(int pintFirstNumber, int pintSecondNumber)
        {
            return pintFirstNumber / pintSecondNumber;
        }

    }
}