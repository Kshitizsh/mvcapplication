﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcApplicationTest.Controllers;

namespace MvcApplicationTest.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {
        [TestMethod]
        public void Get()
        {
            // Arrange
            ValuesController controller = new ValuesController();

            // Act
            IEnumerable<string> result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count());
            Assert.AreEqual("value1", result.ElementAt(0));
            Assert.AreEqual("value2", result.ElementAt(1));
            Assert.AreNotEqual("value3", result.ElementAt(1));
            Assert.AreEqual("value3", result.ElementAt(2));
        }

        [TestMethod]
        public void GetById()
        {
            // Arrange
            ValuesController controller = new ValuesController();

            // Act
            string result = controller.Get(5);

            // Assert
            Assert.AreEqual("value", result);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count()> 0);
        }

        [TestMethod]
        public void VerifySummation()
        {
            // Arrange
            ValuesController controller = new ValuesController();
            const int intFirstNumber = 15;
            const int intsecondNumber = 10;
            var intResult = controller.AddNumbers(intFirstNumber, intsecondNumber);
            Assert.AreEqual(15 + 10, intResult);
            Assert.IsNotNull(intResult);
            Assert.IsTrue(intResult > 0);
        }

        [TestMethod]
        public void VerifySubtraction()
        {
            // Arrange
            ValuesController controller = new ValuesController();
            const int intFirstNumber = 15;
            const int intsecondNumber = 10;
            var intResult = controller.SubractNumbers(intFirstNumber, intsecondNumber);
            Assert.AreEqual(15 - 10, intResult);
            Assert.IsNotNull(intResult);
            Assert.IsTrue(intResult > 0);
        }

    }
}
